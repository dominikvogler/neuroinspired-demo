import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

def get_black_white_image(path: str, size: tuple[int, int] = None):
    if size is not None:
        im = np.array(Image.open(path).convert('L').resize(size))
    else:
        im = np.array(Image.open(path).convert('L'))
    return im

def blacken_out(img: np.ndarray, portion: float = 0.5) -> np.ndarray:
    X,Y = np.shape(img)
    img[int(0.5*128):,:] = 1
    return img

def convert_for_hopfield(img):
    X, Y = np.shape(img)
    img = img.astype(np.int16)
    img[img > 128] = -1
    img[img >= 0] = 1
    return img.astype(np.int8)
    
if __name__ == '__main__':
    path = './img/feather_bw_1.png'
    # path2 = './img/icons8-penguin-50.png'
    path = './img/qr_1.png'
    path = './img/safari.png'

    img = get_black_white_image(path, (128, 128))
    img = img.astype(np.int16)
    print(img)
    img[img > 128] = -1
    img[img >= 0] = 1
    img = img.astype(np.int8)
    print(img)
    

    fig = plt.figure()
    im = plt.imshow(img, cmap='gist_gray_r', vmin=0, vmax=1)
    plt.show()

    