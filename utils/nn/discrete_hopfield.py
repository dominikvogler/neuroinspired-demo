import numpy as np

class DiscreteHopfield():
    def __init__(self, N: int):
        self.N = N
        self.states = self.init_states()   # We only need -1, (0), 1
        self.weights = None

    def init_states(self):
        # We will overwrite this anyway
        return np.ones((self.N, 1), dtype=np.int8)

    def train(self, pattern_data: np.ndarray) -> np.ndarray[np.float32]:
        """
        Trains the network on an array containing the patterns to be learned
        Returns learned weights, but also changes them in place
        
        Args:
            pattern_data (np.ndarray): Assume dimensions (P, N) with P being the number of patters, N == self.N
        """
        # Expand single pattern
        if len(np.shape(pattern_data)) == 1:
            pattern_data = pattern_data.reshape(1, -1)

        P, N = np.shape(pattern_data)
        # print(P, N, self.N)
        if N != self.N:
            raise ValueError(f'Pattern data should be of length {self.N}, is: {N}.')
        
        # Reshape, cast to float, sum over all patterns P, normalize with 1/N scalar
        # print(f'np.shape(np.transpose(pattern_data)): {np.shape(np.transpose(pattern_data))}, pattern_data: {np.shape(pattern_data)}')

        # print(f'matmul (shape): {np.shape(np.matmul(np.transpose(pattern_data).astype(np.float32), pattern_data.astype(np.float32)))}')

        # print(f'matmul: {np.matmul(np.transpose(pattern_data).astype(np.float32), pattern_data.astype(np.float32))}')

        weight_matrix = np.matmul(np.transpose(pattern_data).astype(np.float32), pattern_data.astype(np.float32))
        # print(f'weight_matrix({np.shape(weight_matrix)}): {weight_matrix}')
        self.weights = (1/N) * weight_matrix # TODO: Maye 1/N ?
        np.fill_diagonal(self.weights, 0)

        # print(f'Weights after initialization: np.shape(self.weights): {np.shape(self.weights)} {self.weights}')

        return self.weights

    def run_sync_(self, test_data: np.ndarray):
        activations = np.matmul(self.weights, self.states.astype(np.float32))
        activations[activations >= 0] = 1
        activations[activations < 0] = -1

        changed = (self.states != activations)
        self.states = activations
        return changed          # If any(changed) == False, we have reached equilibrium

    def run_async_(self, test_data: np.ndarray, rounds: int) -> None:
        # changed = False

        # Run one neuron at the time
        for _ in range(rounds):
            # print(f'run_async_: self.states = {self.states}')
            neuron_idx = np.random.randint(0, self.N)
            # print(f'Weights: {np.shape(self.weights)}')

            activation = self.weights[neuron_idx, :] @ test_data
            activation[activation >= 0] = 1
            activation[activation < 0] = -1

            # changed = (activation != self.states[neuron_idx])
            # print(f'{np.squeeze(self.states)}[{neuron_idx}] = ({self.states[neuron_idx]}) -> {activation} (Changed: {changed})')
            self.states[neuron_idx] = activation
        
    def run(self, test_data: np.ndarray, rounds: int = 1, FLAG_SYNC: bool = False) -> np.ndarray:
        """_summary_

        Args:
            test_data (np.ndarray): _description_
            rounds (int): _description_
            FLAG_SYNC (bool, optional): _description_. Defaults to None.

        Raises:
            ValueError: _description_
            ValueError: _description_

        Returns:
            np.ndarray: _description_
        """
        # if self.weights == None:
        #     raise ValueError('Net not initialized yet!')
        if len(np.shape(test_data)) == 1:
            test_data = test_data.reshape(-1, 1)
        if np.shape(test_data) != (self.N, 1):
            raise ValueError(f'Input pattern not correct, should be ({self.N}, 1), is: {np.shape(test_data)}.')
        
        if FLAG_SYNC:
            return self.run_sync_(test_data)
        return self.run_async_(test_data, rounds)

    def get_energy(self) -> float:
        # energy = (-1/2) * np.sum(self.states.reshape(-1, 1) * self.weights * self.states)
        energy = (-1/2) * np.sum(np.matmul(np.matmul(np.transpose(self.states), self.weights), self.states))
        # print(energy)
        return energy
        
        