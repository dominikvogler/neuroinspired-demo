import numpy as np
from utils.nn.discrete_hopfield import DiscreteHopfield

def test():
    faulty_train    = np.array([-1, 1, 1, 1,-1, 1, 1,-1,-1,-1], dtype=np.float32)
    faulty_test     = np.array([1, 1, 1, 1, 1, 1,-1, 1, 1,-1], dtype=np.float32)
    init_img = faulty_test.reshape(-1, 1)

    N = 10
    hopfield = DiscreteHopfield(N)
    hopfield.train(faulty_train)
    hopfield.run(faulty_test, rounds=10000, FLAG_SYNC=False)

    # Output
    states = np.squeeze(hopfield.states)
    print(f'STATES_SHAPE: \t {np.shape(hopfield.states)}')
    print(f'TEST_PATTERN:  \t {faulty_test}')
    print(f'TRAIN_PATTERN: \t {faulty_train}')
    print(f'STATES: \t {states}')
    print((faulty_train == states))
    print((faulty_train == (-1) * states))
    print(f'is_equal / is opposite: {(faulty_train == states).all()} /  {(faulty_train ==  (-1) * states).all()}')

if __name__ == '__main__':
    test()