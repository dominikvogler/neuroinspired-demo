import numpy as np
from utils.nn.discrete_hopfield import DiscreteHopfield

def test():
    # Create patterns
    # pattern = np.array([[-1,1,1,1,1,-1,-1,-1,-1,-1], [-1,-1,-1,-1,-1,1,1,1,1,1], [1, -1, 1, -1, 1, -1, 1, -1, 1, -1]], dtype=np.float32)
    # pattern2 = np.array([1,1,1,1,1,-1,-1,-1,-1,-1], dtype=np.float32)
    # test_pattern = np.array([1,1,1,1,1,-1,-1,-1,-1,-1], dtype=np.float32)
    # np.random.shuffle(test_pattern)

    # Initialize, train Hopfield net
    # hopfield_net = DiscreteHopfield(10)
    # hopfield_net.train(pattern2)

    # # Test hopfield net
    # print(f'STORED PATTERN_2: {pattern2}')
    # print(f'TEST_PATTERN: {test_pattern}')
    # hopfield_net.run(test_pattern, rounds=100, FLAG_SYNC=False)
    # print(f'np.transpose(HOPFIELD_NET.STATES): {np.transpose(hopfield_net.states)}')
    # Initialize, train Hopfield net

    N = 10
    hopfield_net = DiscreteHopfield(N)
    test_case_success = []
    test_cases = []

    for _ in range(10):
        train_pattern = np.random.randint(0, 2, (N))
        train_pattern[train_pattern <= 0] = -1

        test_pattern = np.random.randint(0, 2, (N))
        test_pattern[test_pattern <= 0] = -1

        hopfield_net.init_states()
        hopfield_net.train(train_pattern)

        hopfield_net.init_states()
        hopfield_net.run(test_pattern, rounds=1000, FLAG_SYNC=True)
        result = np.squeeze(hopfield_net.states)
        print(f'===== BEGIN RUN =====')
        # print(f'STATES_SHAPE: \t {np.shape(hopfield_net.states)}')
        print(f'TEST_PATTERN:  \t {test_pattern}')
        print(f'TRAIN_PATTERN: \t {train_pattern}')
        print(f'STATES: \t {result}, SUM: {np.sum(train_pattern + test_pattern)}')

        test_cases.append((train_pattern, test_pattern, result))

        print((train_pattern == result), (train_pattern == (-1) * result))
        is_train = (train_pattern == result).all()
        is_train_opposite = ((train_pattern ==  (-1) * result).all())
        print(f'is_equal / is opposite: {is_train} / {is_train_opposite}')
        test_case_success.append((is_train or is_train_opposite))

    print(f'===== SUMMARY =====')
    print(test_case_success)
    print(all(test_case_success))
    failed = [test_cases[i] for i, success in enumerate(test_case_success) if success == False]
    for train, test, res in failed:
        print(f"NEW CASE")
        print(train)
        print(test)
        print(res)
        print(np.sum(train + test))

if __name__ == '__main__':
    test()