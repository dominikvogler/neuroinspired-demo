import numpy as np
import matplotlib.pyplot as plt

from time import sleep, time
from functools import partial
from matplotlib import animation

from utils.nn.discrete_hopfield import DiscreteHopfield
from utils.visual.image_conversion import get_black_white_image, convert_for_hopfield, blacken_out

def rand_start(X: int, Y: int) -> np.ndarray:
    rand_arr = np.random.randint(0, 2, (X*Y))
    rand_arr[rand_arr <= 0] = -1
    init_img = rand_arr.reshape(X,Y)
    return init_img

def test():
    X, Y, = 128, 128
    N = X*Y
    STEP_SIZE:  int = X*4
    FRAMES:     int = 400
    FPS:        int = 8

    # path = './img/feather_bw_1.png'
    # path3 = './img/qr_2.png'
    path = './img/safari.png'

    img = get_black_white_image(path, (X, Y))
    img = convert_for_hopfield(img)
    X,Y = np.shape(img)

    init_imgs = [rand_start(X,Y) for _ in range(3)]

    hopfield = DiscreteHopfield(N)
    train_data_1 = np.stack(img.flatten())
    print(np.shape(train_data_1))
    hopfield.train(train_data_1)

    # hopfield2 = DiscreteHopfield(N)
    # hopfield2.train(img2.flatten())

    # hopfield3 = DiscreteHopfield(N)
    # hopfield3.train(img3.flatten())

    # hopfield4 = DiscreteHopfield(N)
    # # train_data_4 = np.stack((img.flatten(), img2.flatten(), img3.flatten()), axis=0)
    # train_data_4 = img.flatten()
    # print(np.shape(train_data_4))
    # hopfield4.train(train_data_4)

    fig, (ax0, ax1) = plt.subplots(2)
    # fig, (ax2) = plt.subplots(1, 1)
    # # intialize two line objects (one in each axes)
    # line, = ax1.plot([0], [1])

    # ax1.set_ylim(-1e3, 1e3)
    # ax1.set_xlim(0, 1e3)
    # ax1.grid()

    # ax0.title.set_text(f'feather_bw_1.png ({X}x{Y})')
    # ax1.title.set_text(f'safari.png ({X}x{Y})')
    
    ax0.title.set_text(f'Masked Image')
    # ax0.title.set_text(f'Actual Image')
    ax1.title.set_text(f'Hopfield Network State')

    print("IMG",img)
    # init_img = blacken_out(np.copy(img))
    init_img2 = blacken_out(img.copy())
    print("IMG after", img)
    print("BLACKENED_IMG after", init_img2)
    im = ax0.imshow(init_img2, cmap='gist_gray_r', vmin=0, vmax=1)
    im2 = ax1.imshow(init_img2, cmap='gist_gray_r', vmin=0, vmax=1)
    # line2, = ax2.plot([], [], lw=2)
    # im4 = ax3.imshow(init_img, cmap='gist_gray_r', vmin=0, vmax=1)
    
    xdata, y1data = [], []

    def init_img_anim():
        im2.set_data(init_img2)
        # ax1.set_ylim(-6e5, 0)
        # ax1.set_xlim(0, 1e2)
        # ax1.grid()
        # # ax2.set_xscale('symlog')
        # ax1.set_yscale('symlog')

    def animate_img(i: int):
        hopfield.run(hopfield.states, rounds=STEP_SIZE, FLAG_SYNC=False)
        im2.set_data(hopfield.states.reshape(X,Y))

        # animate3(i)
        return im

    def init3():
        # Initialize plot
        pass
    
    def animate3(i):
        xdata.append(i)
        hopfield.run(hopfield.states, rounds=STEP_SIZE, FLAG_SYNC=False)
        energy = hopfield.get_energy()
        if energy < ax2.get_ylim()[0]:
            ax2.set_ylim(bottom=energy * 1.5)
        if i > ax2.get_xlim()[1]:
            ax2.set_xlim(0, 2*i)
        y1data.append(energy)

        line2.set_data(xdata, y1data)
        # return line2

    # anim: list = [animation.FuncAnimation(fig, animate3, repeat=False, init_func=init3, frames=FRAMES, interval=1)]
    anim = list()

    for hopfield_net, im, init_val in zip([hopfield], [img], [init_img2]):
        # init_func = partial(init_img_anim, im=im, init_img=init_val)
        anim.append(animation.FuncAnimation(fig, animate_img, repeat=False, init_func=init_img_anim, frames=FRAMES, interval=1))
    plt.show()
    # for i, ani in enumerate(anim):
    #     i += 2
    #     print(f'Saving Animation {i+1} ...')
    #     f = f"./saved_data/ani{i+1}_frames{FRAMES}_fps{FPS}_{int(time())}.gif" 
    #     writergif = animation.PillowWriter(fps=FPS) 
    #     ani.save(f, writer=writergif)
    #     print(f'Animation {i+1} saved!')

if __name__ == '__main__':
    test()