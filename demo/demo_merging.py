import numpy as np
import matplotlib.pyplot as plt

from time import sleep, time
from functools import partial
from matplotlib import animation

from utils.nn.discrete_hopfield import DiscreteHopfield
from utils.visual.image_conversion import get_black_white_image, convert_for_hopfield

def rand_start(X: int, Y: int) -> np.ndarray:
    rand_arr = np.random.randint(0, 2, (X*Y))
    rand_arr[rand_arr <= 0] = -1
    init_img = rand_arr.reshape(X,Y)
    return init_img

def test():
    X, Y, = 128, 128
    N = X*Y
    STEP_SIZE:  int = X*4
    FRAMES:     int = 400
    FPS:        int = 8

    path = './img/feather_bw_1.png'
    path3 = './img/qr_2.png'
    path2 = './img/safari.png'

    img = get_black_white_image(path, (X, Y))
    img = convert_for_hopfield(img)
    X,Y = np.shape(img)

    img2 = get_black_white_image(path2, (X, Y))
    img2 = convert_for_hopfield(img2)
    X,Y = np.shape(img2)

    img3 = get_black_white_image(path3, (X, Y))
    img3 = convert_for_hopfield(img3)
    X,Y = np.shape(img3)

    init_imgs = [rand_start(X,Y) for _ in range(3)]

    hopfield = DiscreteHopfield(N)
    train_data_1 = np.stack(img3.flatten())
    print(np.shape(train_data_1))
    hopfield.train(train_data_1)

    hopfield2 = DiscreteHopfield(N)
    hopfield2.train(img2.flatten())

    hopfield3 = DiscreteHopfield(N)
    hopfield3.train(img3.flatten())

    hopfield4 = DiscreteHopfield(N)
    train_data_4 = np.stack((img.flatten(), img2.flatten(), img3.flatten()), axis=0)
    print(np.shape(train_data_4))
    hopfield4.train(train_data_4)

    fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(2, 2)
    # fig, (ax2) = plt.subplots(1, 1)
    # # intialize two line objects (one in each axes)
    # line, = ax1.plot([0], [1])

    # ax1.set_ylim(-1e3, 1e3)
    # ax1.set_xlim(0, 1e3)
    # ax1.grid()

    # ax0.title.set_text(f'feather_bw_1.png ({X}x{Y})')
    # ax1.title.set_text(f'safari.png ({X}x{Y})')
    ax2.title.set_text(f'Energy ({X}x{Y})')

    im = ax0.imshow(init_imgs[0], cmap='gist_gray', vmin=0, vmax=1)
    im2 = ax1.imshow(init_imgs[1], cmap='gist_gray', vmin=0, vmax=1)
    line2, = ax2.plot([], [], lw=2)
    im4 = ax3.imshow(init_imgs[2], cmap='gist_gray', vmin=0, vmax=1)
    
    xdata, y1data = [], []

    def init_img_anim(im, init_img: np.ndarray):
        im.set_data(init_img)

    def animate_img(i: int, hopfield: DiscreteHopfield, im, energy_plot = None):
        if i == 1:
            sleep(0.5)
        hopfield.run(hopfield.states, rounds=STEP_SIZE, FLAG_SYNC=False)
        im.set_data(hopfield.states.reshape(X,Y))

        # TODO: Do something
        if energy_plot is not None:
            pass
        return im

    def init3():
        # Initialize plot
        ax2.set_ylim(-6e5, 0)
        ax2.set_xlim(0, 1e2)
        ax2.grid()
        # ax2.set_xscale('symlog')
        ax2.set_yscale('symlog')
    
    def animate3(i):
        xdata.append(i)
        hopfield3.run(hopfield3.states, rounds=STEP_SIZE, FLAG_SYNC=False)
        energy = hopfield3.get_energy()
        if energy < ax2.get_ylim()[0]:
            ax2.set_ylim(bottom=energy * 1.5)
        if i > ax2.get_xlim()[1]:
            ax2.set_xlim(0, 2*i)
        y1data.append(energy)

        line2.set_data(xdata, y1data)
        return line2

    anim: list = [animation.FuncAnimation(fig, animate3, repeat=False, init_func=init3, frames=FRAMES, interval=1)]
    
    for hopfield_net, im, init_val in zip([hopfield, hopfield2, hopfield4], [im, im2, im4], init_imgs):
        init_func = partial(init_img_anim, im=im, init_img=init_val)
        anim.append(animation.FuncAnimation(fig, animate_img, fargs=(hopfield_net, im), repeat=False, init_func=init_func, frames=FRAMES, interval=1))
    plt.show()
    # for i, ani in enumerate(anim):
    #     i += 2
    #     print(f'Saving Animation {i+1} ...')
    #     f = f"./saved_data/ani{i+1}_frames{FRAMES}_fps{FPS}_{int(time())}.gif" 
    #     writergif = animation.PillowWriter(fps=FPS) 
    #     ani.save(f, writer=writergif)
    #     print(f'Animation {i+1} saved!')

if __name__ == '__main__':
    test()