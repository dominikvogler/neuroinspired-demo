# neuroinspired-demo

The two Hopfield-realated figures in the paper were created with 

* `demo_convergence.py`: First Figure
* `demo_merging.py`: Second figure

To execute, run
```
    python3 -m demo.demo_convergence
    python3 -m demo.demo_merging
```
or, if the `python` command uses Python 3.x:
```
    python -m demo.demo_convergence
    python -m demo.demo_merging
```